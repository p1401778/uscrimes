import pandas as pd
from Data.constants import secteur, sec
import numpy as np
from Cenergetics.Graph import Graph
import pickle

class MetaData:
    def __init__(self, file):
        self.file = file

        self.extract_from_file()

    def extract_from_file(self):
        with open(self.file, 'r') as csv_file:
            self.raw_data = pd.read_csv(csv_file, delimiter=',')
            self.raw_data = self.raw_data[sec]

        pass

    def get_graph(self, n_zones, min_lat, max_lat, min_long, max_long):

        print(len(self.raw_data))

        #self.raw_data[self.raw_data.LON < min_long]
        #self.raw_data[self.raw_data.LON > max_long]
        #self.raw_data[self.raw_data.LAT < min_lat]
        #self.raw_data[self.raw_data.LAT > max_lat]

        #indexNames1 = self.raw_data[(self.raw_data['LON'] > min_long) & (self.raw_data['LON'] < max_long)].index
        #self.raw_data.drop(indexNames1, inplace=True)
        #indexNames2 = self.raw_data[(self.raw_data['LAT'] > min_lat) & (self.raw_data['LAT'] < max_lat)].index
        #self.raw_data.drop(indexNames2, inplace=True)



        print(len(self.raw_data))



        graph_list = [[{id_: 0 for id_ in secteur} for j in range(n_zones)] for i in range(n_zones)]

        print(graph_list)

        # Pas de quadrillage
        pas_lat = (max_lat - min_lat) / n_zones
        pas_long = (max_long - min_long) / n_zones

        g = Graph(n_zones, pas_lat, pas_long)

        print("Latitude : ", min_lat, max_lat)
        print("Longitude : ", min_long, max_long)
        print("Pas : ", pas_lat, pas_long)

        for index, row in self.raw_data.iterrows():

            #print("lol : ", row.LAT, row.LON)

            index_lat = int(np.ceil((row.LAT - min_lat) / pas_lat) - 1)
            index_long = int(np.ceil((row.LON - min_long) / pas_long) - 1)

            #if attributes_list[row["OFFENSE_CODE_GROUP"]] != "OSEF":
            #    graph_list[index_lat][index_long][attributes_list[row["OFFENSE_CODE_GROUP"]]] += 1

            #print(index_lat, index_long)


            if 0 <= index_lat < 20 and 0 <= index_long < 20:
                for s in sec:
                    print("info ", index_lat, " ", index_long, " ", row[s], " ", s)
                    graph_list[index_lat][index_long]["sum_" + s] += row[s]
                    graph_list[index_lat][index_long]["nb_" + s] += 1

        return graph_list


if __name__ == '__main__':
    #print("Load data...")
    #parser = Parser("Data/crime.csv")
    #print("Create Graph...")
    #g = parser.get_graph(20)

    #min_lat = parser.raw_data.Lat.min()
    #max_lat = parser.raw_data.Lat.max()
    #min_long = parser.raw_data.Long.min()
    #max_long = parser.raw_data.Long.max()

    min_lat = 42.2324133
    max_lat = 42.39504158
    min_long = -71.17867378
    max_long = -70.96367615

    print("Load data...")
    meta = MetaData("boston.csv")
    print("Create Graph...")
    o = meta.get_graph(20, min_lat, max_lat, min_long, max_long)

    pickle.dump(o, open("../IHM/meta", "wb"))