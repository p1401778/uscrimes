import time

class Cenergetic:
    def __init__(self, V, C, sigma, delta):
        self.C = C
        self.V = V
        self.n_attributes = len(C)
        self.n_vertex = len(V)
        self.R = []
        self.sigma = sigma
        self.delta = delta

    def processPattern(self,S, X, Y):
        t = time.time()
        if X:
            if len(Y) >= self.sigma and self.UB(union(S, X), Y)>= self.delta:
                x = self.argmin(X, Y, S)
                Yprime = []
                for v in Y:
                    if self.valid(union(S, [x]), [v]):
                        Yprime.append(v)

                fcc = self.f(Yprime)
                if isIn(fcc, union(S, X)):
                    self.processPattern(fcc, exclu(X, fcc), Yprime)
                self.processPattern(S, exclu(X, [x]), Y)
        else:
            if len(Y)>= self.sigma and self.WRAcc(S, Y)>= self.delta:
                self.R.append([S, Y])
        print("epoch ", t)

    def f(self, K):
        result = []
        for c in self.C:
            test = True
            for v in K:
                test = test and (self.gain([c], [v])>0)
            if test:
                result.append(c)
        return result

    def argmin(self, X, Y, S):
        result = {x : 0 for x in X}
        for x in X:
            for v in Y:
                if self.valid(union(S, [x]), [v]):
                    result[x] += 1
        return min(result, key=result.get)


    def UB(self, S, K):
        result = 0
        for v in K:
            for x in S:
                result += self.WRAcc([x], [v])
        return result

    def WRAcc(self, S, K):
        if self.valid(S, K):
            return self.A(S, K) * self._sum(K)/self._sum(self.V)
        else:
            return 0

    def valid(self, S, K):
        result = True
        for v in K:
            for s in S:
                result = result and (self.gain([s], [v]) > 0)
        return result

    def gain(self, L, K):
        return self._sum(K, L)/self._sum(K) - self._sum(self.V, L)/self._sum(self.V)

    def A(self, S, K):
        return self.gain(S, K)

    def _sum(self, K, S = None):
        result = 0
        if S:
            for v in K:
                for s in S:
                    result += v.attributes[s]
        else:
            for v in K:
                for attr in v.attributes.values():
                    result += attr

        #if result == 0:
        #    result = 1


        return result


def union(a, b):
    l = []
    for element in a:
        l.append(element)
    for element in b:
        if element not in l:
            l.append(element)
    return l


def isIn(a, b):
    for element in a:
        if element not in b:
            return False
    return True


def exclu(X, f):
    result = []
    for x in X:
        if x not in f:
            result.append(x)
    return result


if __name__ == '__main__':
    a = [1, 2, 3]
    b = [3, 4, 5]
    print(union(a,b))