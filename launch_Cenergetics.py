from Cenergetics.Parser import Parser
import pickle


if __name__ == '__main__':
    print("Load data...")
    parser = Parser("Data/crime.csv")

    print("Create Graph...")
    g = parser.get_graph(20)

    print("Apply Cenergetic")
    g.toJSON()
    g.cenergetics()

    pickle.dump(g, open("IHM/graph", "wb"))