import json
from Data.constants import categories
import os
from time import sleep
from sklearn.cluster import KMeans


class Graph:
    def __init__(self, n_zones, pas_lat, pas_long):
        self.data = [[None for i in range(n_zones)] for j in range(n_zones)]
        self.n_zones = n_zones
        self.vertices = []

        self.patterns = []
        self.pas_lat = pas_lat
        self.pas_long = pas_long
        self.kmeans = None

    def add_vertex(self, row, col, lat, long, attr):
        v = Vertex(long, lat, attr)
        self.data[row][col] = v
        self.vertices.append(v)

        for (dx, dy) in [(-1, 0), (1,0), (0,-1), (0,1)]:
            if 0 <= row+dx <self.n_zones and 0 <= col+dy<self.n_zones:
                if self.data[row + dx][col+dy] :
                    v.neighbours.append(self.data[row + dx][col+dy])
                    self.data[row + dx][col + dy].neighbours.append(v)

    def toJSON(self):
        data = {}
        data["descriptorName"] = "Name"
        data["attributesName"] = categories
        data["vertices"] = []
        for i in range(len(self.vertices)):
            dict = {}
            dict["vertexId"] = str(self.vertices[i].id)
            liste = []
            for key in categories:
                liste.append(self.vertices[i].attributes[key])
            dict["descriptorsValues"] = liste
            data["vertices"].append(dict)

        data['edges'] = []
        for i in range(len(self.vertices)):
            dict = {}
            dict["vertexId"] = str(self.vertices[i].id)
            liste = []
            for v in range(len(self.vertices[i].neighbours)):
                liste.append(str(self.vertices[i].neighbours[v].id))
            dict["connected_vertices"] = liste
            data["edges"].append(dict)

        with open('Cenergetics/dataToJAR.json', 'w') as outfile:
            json.dump(data, outfile)

    def cenergetics(self):
        os.remove("Cenergetics/results/retrievedPatterns.json")
        os.remove("Cenergetics/results/statistics.txt")
        os.remove("Cenergetics/results/summary.json")
        os.rmdir("Cenergetics/results")
        os.system("java -jar Cenergetics/Cenergetics.jar Cenergetics/parameters.txt")
        sleep(2)

        with open("Cenergetics/results/retrievedPatterns.json", 'r') as json_file:
            data = json.load(json_file)
            for pattern in data["patterns"]:
                charac = pattern['characteristic']['positiveAttributes']
                graph = []
                for g in pattern['subgraph']:
                    graph.append(int(g))
                self.patterns.append([charac, graph])

    def get_values(self, crit):
        values = []
        for v in self.vertices:
            values.append(v.attributes[crit])
        return values

    def get_cluster(self, n_clusters = 3):
        data = []
        for v in self.vertices:
            data.append(list(v.attributes.values()))
        self.kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(data)

        cluster = []
        for v in self.vertices:
            cluster.append(self.kmeans.predict([list(v.attributes.values())]))
        return cluster

class Vertex:
    cpt = 0

    def __init__(self, long, lat, attributes):
        self.id = Vertex.cpt
        Vertex.cpt = Vertex.cpt +1
        self.long = long
        self.lat = lat
        self.attributes = attributes
        self.neighbours = []

    def get_attr_sum(self, attr):
        s = 0
        for a in attr:
            s += self.attributes[a]
        return str(s)