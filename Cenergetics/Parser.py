import pandas as pd
import numpy as np

from Data.constants import categories, attributes_list
from Cenergetics.Graph import Graph

class Parser:
    def __init__(self, file_loc):
        self.file_loc = file_loc
        self.raw_data = None

        self.extract_from_file()

    def extract_from_file(self):
        with open(self.file_loc, 'r') as csv_file:
            self.raw_data = pd.read_csv(csv_file, delimiter=',')
            self.raw_data = self.raw_data[["OFFENSE_CODE_GROUP", "Lat", "Long"]]
            self.raw_data = self.raw_data.dropna()

            self.raw_data = self.raw_data[self.raw_data.Lat != -1]

    def get_graph(self, n_zones):
        graph_list = [[{id_: 0 for id_ in categories} for j in range(n_zones)] for i in range(n_zones)]

        # Calcul des extremums
        min_lat = self.raw_data.Lat.min()
        max_lat = self.raw_data.Lat.max()
        min_long = self.raw_data.Long.min()
        max_long = self.raw_data.Long.max()

        # Pas de quadrillage
        pas_lat = (max_lat - min_lat) / n_zones
        pas_long = (max_long - min_long) / n_zones

        g = Graph(n_zones, pas_lat, pas_long)

        print("Latitude : ", min_lat, max_lat)
        print("Longitude : ", min_long, max_long)
        print("Pas : ", pas_lat, pas_long)

        for index, row in self.raw_data.iterrows():
            index_lat = int(np.ceil((row.Lat - min_lat)/pas_lat) -1)
            index_long = int(np.ceil((row.Long - min_long)/pas_long) -1)

            if attributes_list[row["OFFENSE_CODE_GROUP"]] != "NT":
                graph_list[index_lat][index_long][attributes_list[row["OFFENSE_CODE_GROUP"]]] += 1

        for row in range(n_zones):
            for col in range(n_zones):
                if sum(graph_list[row][col].values())> 0:
                    lat = (row +1)*pas_lat + min_lat
                    long = (col+1)*pas_long + min_long
                    g.add_vertex(row, col, lat, long, graph_list[row][col])

        return g
