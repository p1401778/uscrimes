# Analysis of Boston data crimes

Steeven JANNY (11918550)
Pierre CABANIS (11401778)
Adrien GUEDET (11516525)}

## Installation et compilation

### Prérequis :

[Python 3.7]

### Nos fichiers :

```bash
git clone https://forge.univ-lyon1.fr/p1401778/uscrimes.git
cd uscrimes
```

Executer la chaîne de traitement : [launch_Cenergetics.py](launch_Cenergetics.py)

Affichage des résultats dans l'interface : [launch_Interface.py](launch_Interface.py)


## Liens importants

[Site de l'UE](https://perso.liris.cnrs.fr/marc.plantevit/doku/doku.php?id=dm_ds_2019)

[Rapport](JANNY_CABANIS_GUEDET_Rapport_Projet_DataMining.pdf)

[Video](https://youtu.be/cMskgXTyg3s)

[Cenergetics](Mining exceptional closed patterns in attributed graphs.pdf)