import tkinter as tk
import tkinter.ttk as ttk
from Data.constants import categories, sec, signifiant_sec
import numpy as np


class InfoBox:
    def __init__(self, parent, master):
        self.parent= parent
        self.master = master
        self.title = tk.StringVar()
        self.title.set("")

        self.centers = {c: tk.StringVar() for c in categories}
        self.crimes = {c : tk.StringVar() for c in categories}

        for c in categories:
            self.crimes[c].set("")
            self.centers[c].set("")

    def plot1(self):
        ttk.Label(self.parent, textvariable=self.title).pack(side="top")

        attributes_values = ttk.LabelFrame(self.parent, text="Crimes", width=500, height=150)
        attributes_values.pack_propagate(False)
        attributes_values.pack(side="top")
        for c in categories:
            if c != "NT":
                f = ttk.Frame(attributes_values, width=500, height=15)
                f.pack_propagate(False)
                f.pack(side="top")

                ttk.Label(f, text=c+" : ").pack(side="left")
                ttk.Label(f, textvariable=self.crimes[c]).pack(side="left")

    def plot2(self):
        attributes_values = ttk.LabelFrame(self.parent, text="K-means", width=500, height=400)
        attributes_values.pack_propagate(False)
        attributes_values.pack(side="top")

        for c in categories:
            f = ttk.Frame(attributes_values, width=500, height=15)
            f.pack_propagate(False)
            f.pack(side="top")

            ttk.Label(f, text=c+" : ").pack(side="left")
            ttk.Label(f, textvariable = self.centers[c]).pack(side="left")

    def get_data(self,v,center):
        self.title.set(str(v.lat) + "    "+str(v.long))

        for i, c in enumerate(categories):
            self.crimes[c].set(str(v.attributes[c]))
            if center is not None:
                self.centers[c].set(str(round(center[0][i])))