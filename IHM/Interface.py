import tkinter as tk
import tkinter.ttk as ttk
import pickle
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.colors import hsv_to_rgb
import numpy as np
from IHM.Info_Box import InfoBox
from Data.constants import categories


class Interface(tk.Frame):

    def __init__(self, window, data):
        self.window = window
        tk.Frame.__init__(self, window, width=1500, height=800, bg='#dcdad5')  # Frame principal
        self.pack_propagate(False)
        self.pack(fill=tk.BOTH)

        self.data = data
        self.fig = plt.Figure(figsize=(15, 15), dpi=50)

        self.n_cluster = tk.IntVar()
        self.n_cluster.set(3)

        self.ax = self.fig.add_subplot(111)
        self.canvas = None
        self.last = None

        self.combo = None
        self.combo2 = None
        self.info = None
        self.infobox = None
        self.create_gui()

    def onclick(self, event):
        if event.xdata and event.ydata:
            index_lat = int(np.ceil((event.ydata - 42.2324133) / self.data.pas_lat) - 1)
            index_long = int(np.ceil((event.xdata + 71.17867378) / self.data.pas_long) - 1)

            v = self.data.data[index_lat][index_long]

            center = None
            if self.data.kmeans:
                cluster = self.data.kmeans.predict([list(v.attributes.values())])
                center = self.data.kmeans.cluster_centers_[cluster]
            if v:
                if self.last == 1:
                    self.reprint(None)
                elif self.last == 2:
                    self.reprint_color(None)
                elif self.last == 3:
                    self.clustering()
                self.infobox.get_data(v, center)
                r = Rectangle((v.long, v.lat), -self.data.pas_long, -self.data.pas_lat, color='blue', alpha=0.5)
                self.ax.add_patch(r)
                self.canvas.draw()

    def create_gui(self):
        right = tk.Frame(self, width=300, height=800)
        left = tk.Frame(self, width=1200, height=800)

        right.pack_propagate(False)
        left.pack_propagate(False)

        right.pack(side="right")
        left.pack(side="left")

        top_left = tk.Frame(left, width=1200, height=700)
        bottom_left = tk.Frame(left, width=1200, height=100)

        top_left.pack_propagate(False)
        bottom_left.pack_propagate(False)

        top_left.pack(side="top")
        bottom_left.pack(side="top")

        self.create_selector(bottom_left)
        self.infobox = InfoBox(right, self)
        self.infobox.plot1()
        self.infobox.plot2()

        self.canvas = FigureCanvasTkAgg(self.fig, master=top_left)  # A tk.DrawingArea.
        self.reprint(None)
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

    def create_selector(self, frame):
        selector_frame = ttk.Frame(frame)
        selector_frame.pack(side='top')

        ttk.Label(selector_frame, text="CENERGETIC Mining : ").pack(side="left")
        values = [' '.join(charac[0]) for charac in self.data.patterns]
        self.combo = ttk.Combobox(selector_frame, values=values, width=40)
        self.combo.bind("<<ComboboxSelected>>", self.reprint)
        self.combo.pack(side=tk.LEFT, padx=10)

        color_crime_frame = ttk.Frame(frame)
        color_crime_frame.pack(side="top")
        ttk.Label(color_crime_frame, text="Crime selection : ").pack(side="left")

        values = categories
        self.combo2 = ttk.Combobox(color_crime_frame, values=values, width=20)
        self.combo2.bind("<<ComboboxSelected>>", self.reprint_color)
        self.combo2.pack(side=tk.LEFT, padx=10)

        cluster_frame = ttk.Frame(frame)
        cluster_frame.pack(side='top')

        ttk.Button(cluster_frame, text="Clustering", command=self.clustering).pack(side='left')
        ttk.Entry(cluster_frame, textvariable=self.n_cluster).pack(side='left')

    def create_infobox(self, frame):
        ttk.Label()

    def reprint(self, event):
        self.last = 1
        self.ax.clear()
        self.ax.axis('off')
        BBox = (-71.17867378, -70.96367615,
                42.2324133, 42.39504158)

        im = plt.imread("IHM/map.png")
        self.ax.imshow(im, zorder=0, extent=BBox, aspect="equal")

        for v in self.data.vertices:
            r = Rectangle((v.long, v.lat), -self.data.pas_long, -self.data.pas_lat, fill=None)
            r = self.ax.add_patch(r)
            r.figure.canvas.mpl_connect('button_press_event', self.onclick)
        for pattern in self.data.patterns:
            if ' '.join(pattern[0]) == self.combo.get():
                for v in self.data.vertices:
                    if v.id in pattern[1]:
                        r = Rectangle((v.long, v.lat), -self.data.pas_long, -self.data.pas_lat, color='red', alpha=0.5)
                        self.ax.add_patch(r)
        self.canvas.draw()

    def reprint_color(self, event):
        self.last = 2
        self.ax.clear()
        self.ax.axis('off')
        BBox = (-71.17867378, -70.96367615,
                42.2324133, 42.39504158)

        im = plt.imread("IHM/map.png")
        self.ax.imshow(im, zorder=0, extent=BBox, aspect="equal")

        values = self.data.get_values(self.combo2.get())
        mean = np.mean(values)
        colors = []
        maxi, mini = np.max(values), np.min(values)

        for v in values:
            if v - mean >= 0:
                colors.append(((v - mean) / (maxi - mean), 0, 0))
            else:
                colors.append((0, 0, (v - mean) / (mini - mean)))

        for i, v in enumerate(self.data.vertices):
            r = Rectangle((v.long, v.lat), -self.data.pas_long, -self.data.pas_lat, color=colors[i], alpha=0.5)
            r = self.ax.add_patch(r)
            r.figure.canvas.mpl_connect('button_press_event', self.onclick)

        self.canvas.draw()

    def clustering(self):
        self.last = 3
        cluster = self.data.get_cluster(self.n_cluster.get())
        h = np.linspace(0, 0.7, self.n_cluster.get())
        self.ax.clear()
        self.ax.axis('off')
        BBox = (-71.17867378, -70.96367615,
                42.2324133, 42.39504158)

        im = plt.imread("IHM/map.png")
        self.ax.imshow(im, zorder=0, extent=BBox, aspect="equal")
        for i, v in enumerate(self.data.vertices):
            c = hsv_to_rgb((float(h[cluster[i]]), 1, 1))
            r = Rectangle((v.long, v.lat), -self.data.pas_long, -self.data.pas_lat, color=c, alpha=0.5)
            r = self.ax.add_patch(r)
            r.figure.canvas.mpl_connect('button_press_event', self.onclick)
        self.canvas.draw()


def launch(g):
    fenetre = tk.Tk()
    interface = Interface(fenetre, g)
    interface.mainloop()