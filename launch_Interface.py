import pickle
import tkinter as tk

from IHM.Interface import launch

if __name__ == '__main__':
    g = pickle.load(open("IHM/graph", 'rb'))
    launch(g)
